String.prototype.format = function () {
	var i = 0, args = arguments; 
	return this.replace(/{}/g, function () { 
		return typeof args[i] != 'undefined' ? args[i++] : ''; 
	}); 
};
function jsValidateEmail(input){
	var obj = $(input);
	if (!obj) return false;
	if(!jsIsEmpty(obj)){
		var value = obj.val();
		if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)){
			//jsHighLightInput(obj);
			return false;
		} else {
			//jsResetInput(obj);
			return true;
		}
	}else{
		return false;
	}
}
function jsIsEmpty(input){
	if(input.val().length){
		//jsResetInput(input);
		return false;
	} else {
		//jsHighLightInput(input);
		return true;
	}
}
function jsGetProducto(index){
	var jSON = [{
		'AntheliosXLGelCremaToqueSecoConColorFPS50+': {
			'imagen': '01.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Gel Crema Toque Seco<br> con Color Antibrillo<br> FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel mixta a grasa.',
			'descprod': 'Muy alta protección UVA/UVB de rostro. Absorción instantánea y acabado ultraseco. No deja marcas blancas.',
			'tamanio': '50 ml.',
		}, 'AntheliosBrumaDeRostroUltraligeraFPS50': {
			'imagen': '02.png',
			'nombreproducto': 'Anthelios Bruma',
			'descripcionproducto': 'De Rostro <br>Ultraligera <br>FPS 50',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Todo tipo de piel.',
			'descprod': 'Muy alta protección UVB/UVA de rostro. Textura invisible, absorción instantánea y no deja marcas blancas.',
			'tamanio': '75 ml.',
		}, 'AntheliosSprayFPS50+': {
			'imagen': '03.png',
			'nombreproducto': 'Anthelios',
			'descripcionproducto': 'Spray <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Todo tipo de piel.',
			'descprod': 'Muy alta protección UVA/UVB de cuerpo con textura ultraligera y fluida.',
			'tamanio': '200 ml.',
		}, 'AntheliosXLBrumaDeCuerpoFPS50+': {
			'imagen': '04.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Bruma de Cuerpo<br> Ultraligera <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Todo tipo de piel.',
			'descprod': 'Muy alta protección UVA/UVB de cuerpo. Textura invisible, absorción instantánea y no deja marcas blancas.',
			'tamanio': '200 ml.',
		}, 'AntheliosXLFluidoUltraligeroColorFPS50+': {
			'imagen': '05.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Fluido Ultraligero <br>Color <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel normal a mixta.',
			'descprod': 'Muy alta protección UVB/UVA de rostro. Textura ultraligera, no deja marcas blancas.',
			'tamanio': '50 ml.',
		}, 'AntheliosXLFluidoUltraligeroFPS50+': {
			'imagen': '06.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Fluido <br>Ultraligero <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel normal a mixta.',
			'descprod': 'Muy alta protección UVB/UVA de rostro. Textura ultraligera, no deja marcas blancas.',
			'tamanio': '50 ml.',
		}, 'AntheliosXLCremaConfortFPS50+': {
			'imagen': '07.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Crema <br>Confort <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel normal a seca.',
			'descprod': 'Muy alta protección UVA/UBV de rostro, confort extremo.',
			'tamanio': '50 ml.',
		}, 'AntheliosXLLecheConfortFPS50+': {
			'imagen': '09.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Leche Confort <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel normal a seca.',
			'descprod': 'Muy alta protección UVA/UVB de cuerpo. Confort extremo.',
			'tamanio': '100 ml. /300 ml.',
		}, 'AntheliosXLGelCremaToqueSecoAntibrilloFPS50+': {
			'imagen': '10.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Gel Crema Toque <br>Seco Antibrillo <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel mixta a grasa.',
			'descprod': 'Muy alta protección UVA/UVB de rostro. Absorción instantánea y acabado ultraseco. No deja marcas blancas.',
			'tamanio': '50 ml.',
		}, 'AntheliosUnifiantMousseFPS50+': {
			'imagen': '11.png',
			'nombreproducto': 'Anthelios',
			'descripcionproducto': 'Unifiant <br>Mousse <br>FPS 50',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Todo tipo de piel.',
			'descprod': 'Muy alta protección UVA/UVB de rostro. Textura tipo mousse que difumina los defectos, alisa visiblemente la superficie de la piel y unifica el tono.',
			'tamanio': '40 ml.',
		}, 'AntheliosXLCremaColorFPS50+': {
			'imagen': '12.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Crema Color BB<br> Confort <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel normal a seca.',
			'descprod': 'Muy alta protección UVA/UVB de rostro. Absorción instantánea y acabado ultraseco. No deja marcas blancas.',
			'tamanio': '50 ml.',
		}, 'AntheliosDermopediatricoLecheBebeFPS50+': {
			'imagen': '13.png',
			'nombreproducto': 'Anthelios',
			'descripcionproducto': 'Dermopediátrico <br>Leche Bebé <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': '&nbsp;',
			'descprod': 'Muy alta protección UVA/UVB de rostro y cuerpo. Piel delicada o con tendencia atópica del bebé. Con manteca de Karité.',
			'tamanio': '50 ml.',
		}, 'AntheliosSprayMultiposicionesFPS50+': {
			'imagen': '14.png',
			'nombreproducto': 'Anthelios',
			'descripcionproducto': 'Spray <br>Multiposiciones <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': '&nbsp;',
			'descprod': 'Muy alta protección UVA/UVB de rostro y cuerpo. Bruma fina contínua y uso multiángulo. Textura muy resistente al agua.',
			'tamanio': '125 ml.',
		}, 'AntheliosLecheAterciopeladaFPS50+': {
			'imagen': '15.png',
			'nombreproducto': 'Anthelios',
			'descripcionproducto': 'Leche <br>Aterciopelada <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': '&nbsp;',
			'descprod': 'Muy alta protección UVA/UVB de rostro y cuerpo. Textura muy resistente al agua y de rapida absorción.',
			'tamanio': '100 ml.',
		}, 'AntheliosACFluidoACAntiacneFPS30': {
			'imagen': '17.png',
			'nombreproducto': 'Anthelios AC',
			'descripcionproducto': 'Fluido AC <br>Antiacné <br>FPS 30',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Piel grasa con tendencia acneica.',
			'descprod': 'Protección alta/media UVB/UVA de rostro. Doble acción antibrillo: seboabsorbente y seboregulador.',
			'tamanio': '50 ml.',
		}, 'AntheliosXLCompactoCremaFPS50+': {
			'imagen': 'Crema-claire.png',
			'nombreproducto': 'Anthelios XL',
			'descripcionproducto': 'Compacto-Crema <br>FPS 50+',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Todo tipo de piel.',
			'descprod': 'Muy alta protección UVA/UVB de rostro. Resistente al agua. Disponible en 2 tonos (claro y oscuro).',
			'tamanio': 'Petaca con espejo 9 g.',
		}, 'AntheliosSprayUltraligeroFPS30': {
			'imagen': 'Spray.png',
			'nombreproducto': 'Anthelios',
			'descripcionproducto': 'Spray <br>Ultraligero <br>FPS 30',
			'linkconoce': 'http://www.laroche-posay.com.ar/productos-tratamientos/Anthelios/Piel-sensible-o-con-alergia-solar-r912.aspx',
			'linkdonde': 'http://www.laroche-posay.com.ar/farmacias/internacional/localizador-de-puntos-de-venta-sl.aspx',
			'tipopiel': 'Todo tipo de piel.',
			'descprod': 'Alta protección UVA/UVB de cuerpo con textura ultraligera y fluida. Resistente al agua.',
			'tamanio': '200 ml.',
		}
	}];

	return jSON[0][index];
}
function jsGetTemplateProducto(){
	var templateProductoRecomendado = ''+
					'<ul>'+
						'<li>'+
							'<div class="product">'+
								'<div class="boxes thumb">'+
									'<img src="img/{}" alt="">'+
								'</div>'+
								'<div class="boxes details">'+
									'<span>{}</span>'+
									'<span>'+
										'<span class="strong">{}</span>'+
										'{}'+
									'</span>'+
								'</div>'+
							'</div>'+
						'</li>'+
						'<li>'+
							'<ul class="info">'+
								'<li>'+
									'<a href="{}" target="_blank">Conocé más</a>'+
								'</li>'+
								'<li>'+
									'<a href="{}" target="_blank">Dónde comprar</a>'+
								'</li>'+
							'</ul>'+
						'</li>'+
						'<li>'+
							'<span>{}</span>'+
						'</li>'+
						'<li>'+
							'<p>{}</p>'+
						'</li>'+
						'<li>'+
							'<span>{}</span>'+
						'</li>'+
					'</ul>';
	return templateProductoRecomendado;
}
function jsGetTemplateAcompanante(){
	var templateAcompanante = ''+
			'<div class="container limpiar">'+
				'<span class="name">{}</span>'+
				'<div class="box left">'+
					'{}'+
				'</div>'+
				'<div class="box right">'+
					'{}'+
				'</div>'+
			'</div>';
	return templateAcompanante;
}


var cantAcompanantes = '';
var numAcompanante = '';
var arrayAcompanantes = [];
function jsGetRespuestaCuerpo(usrCuerpoTP, usrCuerpoTextP){
	var usrRepCuerpo = '';
	if( usrCuerpoTextP == 'leche' ) usrRepCuerpo = 'AntheliosXLLecheConfortFPS50+';
	if( usrCuerpoTextP == 'spray' ) usrRepCuerpo = 'AntheliosSprayUltraligeroFPS30';
	if( usrCuerpoTextP == 'bruma' ) usrRepCuerpo = 'AntheliosXLBrumaDeCuerpoFPS50+';
	return usrRepCuerpo;
}
function jsGetRespuestaRostro(usrRostroTP, usrRostroC, usrRostroTextP){
	var usrRepRostro = '';
	if(usrRostroC == 'concolor'){
		if( (usrRostroTP == 'normal' || usrRostroTP == 'mixta' || usrRostroTP == 'grasa') && usrRostroTextP == 'toqueseco' ) usrRepRostro = 'AntheliosXLGelCremaToqueSecoConColorFPS50+';
		if( usrRostroTextP == 'mousse' ) usrRepRostro = 'AntheliosUnifiantMousseFPS50+';
		if( usrRostroTextP == 'fluido' ) usrRepRostro = 'AntheliosXLFluidoUltraligeroColorFPS50+';
		if( (usrRostroTP == 'normal' || usrRostroTP == 'seca') && usrRostroTextP == 'crema' ) usrRepRostro = 'AntheliosXLCremaColorFPS50+';
		if( (usrRostroTP == 'normal' || usrRostroTP == 'mixta' || usrRostroTP == 'seca') && usrRostroTextP == 'compacto' ) usrRepRostro = 'AntheliosXLCompactoCremaFPS50+';
	}else if(usrRostroC == 'sincolor'){
		if( (usrRostroTP == 'normal' || usrRostroTP == 'mixta' || usrRostroTP == 'grasa') && usrRostroTextP == 'toqueseco' ) usrRepRostro = 'AntheliosXLGelCremaToqueSecoAntibrilloFPS50+';
		if( usrRostroTextP == 'fluido' ) usrRepRostro = 'AntheliosXLFluidoUltraligeroFPS50+';
		//if( usrRostroTP == 'grasa' && usrRostroTextP == 'fluido' ) usrRepRostro = 'AntheliosACFluidoACAntiacneFPS30';
		if( (usrRostroTP == 'normal' || usrRostroTP == 'seca') && usrRostroTextP == 'crema' ) usrRepRostro = 'AntheliosXLCremaConfortFPS50+';
		if( usrRostroTextP == 'bruma' ) usrRepRostro = 'AntheliosBrumaDeRostroUltraligeraFPS50';
	}
	return usrRepRostro;
		/*'AntheliosSprayMultiposicionesFPS50+'
		'AntheliosDermopediatricoLecheBebeFPS50+'
		'AntheliosLecheAterciopeladaFPS50+'*/
}
function jsShowResultados(){
	var usrNombre = $('#inputFullName').val();
	$('#idNombreUsuario').text('para '+usrNombre);
	//usuario ROSTRO
	var usrRepRostro = '';
	var usrRostroTP = $('#selectRostroTipoPiel').val();
	var usrRostroC = $('#selectRostroColor').val();
	var usrRostroTextP = $('#selectRostroTexturaPreferida').val();

	var dataProd = '';
	var temp = '';

	usrRepRostro = jsGetRespuestaRostro(usrRostroTP, usrRostroC, usrRostroTextP);

	if(usrRepRostro === ''){
		alert('sin combinacion para: '+usrRostroTP+', '+usrRostroC+', '+usrRostroTextP);
	}else{
		dataProd = jsGetProducto(usrRepRostro);
		temp = jsGetTemplateProducto();
		temp = temp.format(
			dataProd.imagen,
			'Recomendación <br>de rostro',
			dataProd.nombreproducto,
			dataProd.descripcionproducto,
			dataProd.linkconoce,
			dataProd.linkdonde,
			dataProd.tipopiel,
			dataProd.descprod,
			dataProd.tamanio
		);
		$('#idUsuarioRecomendacionRostro').html(temp);
	}

	//FIN usuario ROSTRO
	//usuario CUERPO
	var usrRepCuerpo = '';
	var usrCuerpoTP = '';//$('#selectCuerpoTipoPiel').val();
	var usrCuerpoTextP = $('#selectCuerpoTexturaPreferida').val();

	usrRepCuerpo = jsGetRespuestaCuerpo(usrCuerpoTP, usrCuerpoTextP);

	if(usrRepCuerpo === ''){
		alert('sin combinacion para: '+usrCuerpoTP+', '+usrCuerpoTextP);
	}else{
		dataProd = jsGetProducto(usrRepCuerpo);
		temp = jsGetTemplateProducto();
		temp = temp.format(
			dataProd.imagen,
			'Recomendación <br>de cuerpo',
			dataProd.nombreproducto,
			dataProd.descripcionproducto,
			dataProd.linkconoce,
			dataProd.linkdonde,
			dataProd.tipopiel,
			dataProd.descprod,
			dataProd.tamanio
		);
		$('#idUsuarioRecomendacionCuerpo').html(temp);
	}

	//idUsuarioRecomendacionCuerpo

	//FIN usuario CUERPO
	//ACOMPAÑANTES
	if(arrayAcompanantes.length){
		$('.result.one.second').show();
		$('.result.one.second').html('');
	}else $('.result.one.second').hide();

	for (var i = 0; i < arrayAcompanantes.length; i++) {
		var arrayDato = arrayAcompanantes[i];

		var acompananteRepRostro = '';
		var acompananteRostroTP = arrayDato[1];
		var acompananteRostroC = arrayDato[2];
		var acompananteRostroTextP = arrayDato[3];

		var acompananteRepCuerpo = '';
		var acompananteCuerpoTP = arrayDato[4];
		var acompananteCuerpoTextP = arrayDato[5];

		if( arrayDato[0] == 'bebe'){
			//acompañante ROSTRO
			acompananteRepRostro = 'AntheliosDermopediatricoLecheBebeFPS50+';
			//FIN acompañante ROSTRO
			//acompañante CUERPO
			acompananteRepCuerpo = 'AntheliosDermopediatricoLecheBebeFPS50+';
			//FIN acompañante CUERPO
		}else if( arrayDato[0] == 'nino'){
			if(acompananteRostroTextP == 'bruma'){
				//acompañante ROSTRO
				acompananteRepRostro = 'AntheliosSprayMultiposicionesFPS50+';
				//FIN acompañante ROSTRO
				//acompañante CUERPO
				acompananteRepCuerpo = 'AntheliosSprayMultiposicionesFPS50+';
				//FIN acompañante CUERPO
			}else{
				//acompañante ROSTRO
				acompananteRepRostro = 'AntheliosLecheAterciopeladaFPS50+';
				//FIN acompañante ROSTRO
				//acompañante CUERPO
				acompananteRepCuerpo = 'AntheliosLecheAterciopeladaFPS50+';
				//FIN acompañante CUERPO
			}
		}else{
			//acompañante ROSTRO
			acompananteRepRostro = jsGetRespuestaRostro(acompananteRostroTP, acompananteRostroC, acompananteRostroTextP);
			//FIN acompañante ROSTRO
			//acompañante CUERPO
			acompananteRepCuerpo = jsGetRespuestaCuerpo(acompananteCuerpoTP, acompananteCuerpoTextP);
			//FIN acompañante CUERPO
		}

		//acompañante ROSTRO
		if(acompananteRepRostro === ''){
			alert( (i+1)+' sin combinacion para: '+acompananteRostroTP+', '+acompananteRostroC+', '+acompananteRostroTextP);
		}else{
			dataProd = jsGetProducto(acompananteRepRostro);
			temp = jsGetTemplateProducto();
			temp = temp.format(
				dataProd.imagen,
				'Recomendación <br>de rostro',
				dataProd.nombreproducto,
				dataProd.descripcionproducto,
				dataProd.linkconoce,
				dataProd.linkdonde,
				dataProd.tipopiel,
				dataProd.descprod,
				dataProd.tamanio
			);
			acompananteRepRostro = temp;
		}
		//FIN acompañante ROSTRO
		//acompañante CUERPO
		if(acompananteRepCuerpo === ''){
			alert( (i+1)+' sin combinacion para: '+acompananteCuerpoTP+', '+acompananteCuerpoTextP);
		}else{
			dataProd = jsGetProducto(acompananteRepCuerpo);
			temp = jsGetTemplateProducto();
			temp = temp.format(
				dataProd.imagen,
				'Recomendación <br>de cuerpo',
				dataProd.nombreproducto,
				dataProd.descripcionproducto,
				dataProd.linkconoce,
				dataProd.linkdonde,
				dataProd.tipopiel,
				dataProd.descprod,
				dataProd.tamanio
			);
			acompananteRepCuerpo = temp;
		}
		//FIN acompañante CUERPO

		var templateAcom = jsGetTemplateAcompanante();
		templateAcom = templateAcom.format(
			'PARA ACOMPAÑANTE '+ (i+1),
			acompananteRepRostro,
			acompananteRepCuerpo
		);
		$('.result.one.second').append(templateAcom);
	}
	//FIN ACOMPAÑANTES
}
function jsValidarForms(actual, siguiente){
	var strMensaje = '';
	var ok = true;
	if( actual == '.step.first' ){
		if($('#inputFullName').val().replace(/\s/g,'') === '') ok = false;
		if($('#selectSexo').val() === '') ok = false;
		//if($('#selectEdad').val() === '') ok = false;
		if(!jsValidateEmail('#inputEmail')) ok = false;
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}else if( actual == '.step.second' ){
		if($('#selectRostroTipoPiel').val() === '') ok = false;
		if($('#selectRostroTexturaPreferida').val() === '') ok = false;
		if($('#selectRostroColor').val() === '') ok = false;
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}else if( actual == '.step.third' ){
		//if($('#selectCuerpoTipoPiel').val() === '') ok = false;
		if($('#selectCuerpoTexturaPreferida').val() === '') ok = false;
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}else if( actual == '.step.fourth' ){
		if($('#selectDondeVas').val() === '') ok = false;
		if($('#selectCuantosDiasVas').val() === '') ok = false;
		if($('#selectConQuienVas').val() === '') ok = false;
		if(!$('#selectCantidadAcompanantes').parent().hasClass('disabled')){
			if($('#selectCantidadAcompanantes').val() === ''){ 
				ok = false;
			}else{
				cantAcompanantes = parseInt($('#selectCantidadAcompanantes').val());
				numAcompanante = 1;
				$('.spandatosacompanante').text(numAcompanante);
			}
		}
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}else if( actual == '.step.fifth' ){
		if($('#selectAcompananteEdad').val() === '') ok = false;
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}else if( actual == '.step.sixth' ){
		if($('#selectAcompananteRostroTipoPiel').val() === '') ok = false;
		if($('#selectAcompananteRostroColor').val() === '') ok = false;
		if($('#selectAcompananteRostroTexturaPreferida').val() === '') ok = false;
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}else if( actual == '.section.two' ){
		//if($('#selectAcompananteCuerpoTipoPiel').val() === '') ok = false;
		if($('#selectAcompananteCuerpoTexturaPreferida').val() === '') ok = false;
		if(!ok) strMensaje = "Todos los campos son obligatorios.";
	}
	return strMensaje;
}
function jsGoPaso(actual, siguiente, accion){
	var pos = 0;
	var arrayDato = null;
	if( actual !== '' && siguiente !== '' ){
		var strMensaje = '';
		if(accion != 'ant'){
			strMensaje = jsValidarForms(actual, siguiente);

			if( strMensaje === '' ){
				if(actual == '.step.fifth'){
					var valAcomEdad = $('#selectAcompananteEdad').val();
					if(valAcomEdad == 'bebe'){
						actual ='.section.two';
					}
				}


				if( actual == '.step.fourth' ){
					if(cantAcompanantes){
						numAcompanante = 1;
						$('.spandatosacompanante').text(numAcompanante);
					}else{
						jsShowResultados();
						actual = '.section.two';
						siguiente = '.section.three';
					}
				}else if( actual == '.section.two' ){
					var aE = $('#selectAcompananteEdad').val();
					var aRTP = $('#selectAcompananteRostroTipoPiel').val();
					var aRC = $('#selectAcompananteRostroColor').val();
					var aRTPre = $('#selectAcompananteRostroTexturaPreferida').val();
					var aCTP = $('#selectAcompananteCuerpoTipoPiel').val();
					var aCTPre = $('#selectAcompananteCuerpoTexturaPreferida').val();

					var arrayDatosActuales = [aE, aRTP, aRC, aRTPre, aCTP, aCTPre];
					arrayAcompanantes[numAcompanante-1] = arrayDatosActuales;
					if(numAcompanante >= cantAcompanantes){
						jsShowResultados();
						actual = '.section.two';
						siguiente = '.section.three';
					}else{
						numAcompanante++;
						$('.spandatosacompanante').text(numAcompanante);
						actual = '.step.seventh';
						siguiente = '.step.fifth';

						if(numAcompanante <= arrayAcompanantes.length){
							pos = numAcompanante - 1;
							arrayDato = arrayAcompanantes[pos];
							$('#selectAcompananteEdad').val(arrayDato[0]);
							$('#selectAcompananteRostroTipoPiel').val(arrayDato[1]);
							$('#selectAcompananteRostroColor').val(arrayDato[2]);
							$('#selectAcompananteRostroTexturaPreferida').val(arrayDato[3]);
							$('#selectAcompananteCuerpoTipoPiel').val(arrayDato[4]);
							$('#selectAcompananteCuerpoTexturaPreferida').val(arrayDato[5]);
						}else{
							$('#selectAcompananteEdad').val('');
							$('#selectAcompananteRostroTipoPiel').val('');
							$('#selectAcompananteRostroColor').val('');
							$('#selectAcompananteRostroTexturaPreferida').val('');
							$('#selectAcompananteCuerpoTipoPiel').val('');
							$('#selectAcompananteCuerpoTexturaPreferida').val('');
						}

						//console.log(numAcompanante);
					}
				}
			}
		}else{
			if( actual == '.step.fifth' ){
				if(numAcompanante > 1){
					numAcompanante--;
					siguiente = '.step.seventh';
					pos = numAcompanante - 1;
					arrayDato = arrayAcompanantes[pos];
					$('#selectAcompananteEdad').val(arrayDato[0]);
					$('#selectAcompananteRostroTipoPiel').val(arrayDato[1]);
					$('#selectAcompananteRostroColor').val(arrayDato[2]);
					$('#selectAcompananteRostroTexturaPreferida').val(arrayDato[3]);
					$('#selectAcompananteCuerpoTipoPiel').val(arrayDato[4]);
					$('#selectAcompananteCuerpoTexturaPreferida').val(arrayDato[5]);
					$('.spandatosacompanante').text(numAcompanante);

					if(arrayDato[0] == 'bebe')siguiente = '.step.fifth';
				}else{
					//strMensaje = 'POR AHORA NO SE PUEDE';
				}
			}
		}
		


		if(strMensaje === ''){
			if(siguiente == '.section.three'){
				//console.log('MOSTRAR SELECCION');
			}
			$(actual).hide();
			$(siguiente).show();
		}else{
			swal({
				title: strMensaje,
				html: true
			});
		}
	}
}
function jsCheckRostro(TP, C, elemento){
	$(elemento+" option").removeAttr('disabled');
	$(elemento+" option").removeAttr('selected');
	$(elemento+" option").attr('disabled', 'disabled');
	$(elemento+" option:first").attr('selected', 'selected');
	$(elemento+" option:first").removeAttr('disabled');
	if(TP !== '' && C !== ''){
		if(TP == 'normal'){
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			$(elemento+" option[value='crema']").removeAttr('disabled');
			$(elemento+" option[value='toqueseco']").removeAttr('disabled');
			if( C == 'concolor'){
				$(elemento+" option[value='mousse']").removeAttr('disabled');
				$(elemento+" option[value='compacto']").removeAttr('disabled');
			}else if( C == 'sincolor'){
				$(elemento+" option[value='bruma']").removeAttr('disabled');
			}
		}else if(TP == 'mixta'){
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			$(elemento+" option[value='toqueseco']").removeAttr('disabled');
			if( C == 'concolor'){
				$(elemento+" option[value='mousse']").removeAttr('disabled');
				$(elemento+" option[value='compacto']").removeAttr('disabled');
			}else if( C == 'sincolor'){
				$(elemento+" option[value='bruma']").removeAttr('disabled');
			}
		}else if(TP == 'grasa'){
			$(elemento+" option[value='toqueseco']").removeAttr('disabled');
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			if( C == 'concolor'){
				$(elemento+" option[value='mousse']").removeAttr('disabled');
			}else if( C == 'sincolor'){
				$(elemento+" option[value='bruma']").removeAttr('disabled');
			}
		}else if(TP == 'seca'){
			$(elemento+" option[value='crema']").removeAttr('disabled');
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			if( C == 'concolor'){
				$(elemento+" option[value='mousse']").removeAttr('disabled');
				$(elemento+" option[value='compacto']").removeAttr('disabled');
			}else if( C == 'sincolor'){
				$(elemento+" option[value='bruma']").removeAttr('disabled');
			}
		}
	}
}
function jsCheckCuerpo(TP, elemento){
	/*$(elemento+" option").removeAttr('disabled');
	$(elemento+" option").removeAttr('selected');
	$(elemento+" option").attr('disabled', 'disabled');
	$(elemento+" option:first").attr('selected', 'selected');
	$(elemento+" option:first").removeAttr('disabled');
	if(TP !== ''){
		if(TP == 'normal'){
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			$(elemento+" option[value='spray']").removeAttr('disabled');
			$(elemento+" option[value='bruma']").removeAttr('disabled');
		}else if(TP == 'mixta'){
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			$(elemento+" option[value='bruma']").removeAttr('disabled');
		}else if(TP == 'grasa'){
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			$(elemento+" option[value='bruma']").removeAttr('disabled');
		}else if(TP == 'seca'){
			$(elemento+" option[value='fluido']").removeAttr('disabled');
			$(elemento+" option[value='spray']").removeAttr('disabled');
			$(elemento+" option[value='bruma']").removeAttr('disabled');
		}
	}*/
}
$(function(){
	/**/
	$('.section .container a.btn').on('click', function(e){
		var actual = $(this).data('actual');
		var siguiente = $(this).data('siguiente');
		var accion = $(this).data('accion');
		console.log(actual+' '+siguiente+' '+accion);
		/*if( actual !== 'undefined' || siguiente !== 'undefined' || accion !== 'undefined'){
			e.preventDefault();
		}*/
		jsGoPaso(actual, siguiente, accion);
	});
	/**/
	$('#selectRostroTipoPiel').change(function(){
		var valTP = $(this).val();
		var valC = $('#selectRostroColor').val();
		jsCheckRostro(valTP, valC, "#selectRostroTexturaPreferida");
	});
	$('#selectRostroColor').change(function(){
		var valTP = $('#selectRostroTipoPiel').val();
		var valC = $(this).val();
		jsCheckRostro(valTP, valC, "#selectRostroTexturaPreferida");
	});
	$('#selectCuerpoTipoPiel').change(function(){
		var valTP = $(this).val();
		jsCheckCuerpo(valTP, "#selectCuerpoTexturaPreferida");
	});
	$('#selectAcompananteRostroColor').change(function(){
		var val = $(this).val();
		$("#selectAcompananteRostroTexturaPreferida option").removeAttr('disabled');
		$("#selectAcompananteRostroTexturaPreferida option").removeAttr('selected');
		$("#selectAcompananteRostroTexturaPreferida option:first").attr('selected', 'selected');
		if( val == 'concolor'){
			$("#selectAcompananteRostroTexturaPreferida option[value='bruma']").attr('disabled', 'disabled');
		}else if( val == 'sincolor'){
			$("#selectAcompananteRostroTexturaPreferida option[value='mousse']").attr('disabled', 'disabled');
		}
	});
	/**/
	$('#selectAcompananteRostroTipoPiel').change(function(){
		var valTP = $(this).val();
		var valC = $('#selectAcompananteRostroColor').val();
		jsCheckRostro(valTP, valC, "#selectAcompananteRostroTexturaPreferida");
	});
	$('#selectAcompananteRostroColor').change(function(){
		var valTP = $('#selectAcompananteRostroTipoPiel').val();
		var valC = $(this).val();
		jsCheckRostro(valTP, valC, "#selectAcompananteRostroTexturaPreferida");
	});
	$('#selectAcompananteCuerpoTipoPiel').change(function(){
		var valTP = $(this).val();
		jsCheckCuerpo(valTP, "#selectAcompananteCuerpoTexturaPreferida");
	});
	/**/
	$('#selectConQuienVas').change(function(){
		var val = $(this).val();
		if( val == 'acompanado'){
			$('#selectCantidadAcompanantes').removeAttr('disabled');
			$('#selectCantidadAcompanantes').parent().removeClass('disabled');
		}else{
			$("#selectCantidadAcompanantes option").removeAttr('selected');
			$("#selectCantidadAcompanantes option:first").attr('selected', 'selected');
			$('#selectCantidadAcompanantes').attr('disabled', 'disabled');
			$('#selectCantidadAcompanantes').parent().addClass('disabled');
		}
	});
	/**/
	$('#selectAcompananteEdad').change(function(){
		var val = $(this).val();
		if( val == 'bebe'){
			
		}else if( val == 'nino'){
			
		}else if( val == 'adulto'){
			
		}
	});
	/**/
	$('.abtnReiniciarRecomen').on('click', function(){
		$('body *').removeAttr('style');
		$('.section.one').hide();
		$('.section.two').show();

		$('#selectRostroTipoPiel').val('');
		$('#selectRostroColor').val('');
		$('#selectRostroTexturaPreferida').val('');
		$('#selectCuerpoTipoPiel').val('');
		$('#selectCuerpoTexturaPreferida').val('');

		$('#selectDondeVas').val('');
		$('#selectCuantosDiasVas').val('');
		$('#selectConQuienVas').val('');
		$('#selectCantidadAcompanantes').val('');

		$('#selectAcompananteEdad').val('');
		$('#selectAcompananteRostroTipoPiel').val('');
		$('#selectAcompananteRostroColor').val('');
		$('#selectAcompananteRostroTexturaPreferida').val('');
		$('#selectAcompananteCuerpoTipoPiel').val('');
		$('#selectAcompananteCuerpoTexturaPreferida').val('');

		cantAcompanantes = '';
		numAcompanante = '';
		arrayAcompanantes = [];
	});
	/**/
	$(".previus").on('click', function(){
		var slideactual = $('.slider .caption .activo');
		var dotactual = $('.slider .frames .active');

		$('.slider .caption li').removeClass('activo');
		$('.slider .frames li').removeClass('active');

		var slideprev = slideactual.prev('.lislidetexto');
		var dotprev = dotactual.prev('.lidot');

		if(!slideprev.length){
			slideprev = $('.slider .caption li:last-child');
			dotprev = $('.slider .frames li:last-child');
		}

		slideprev.addClass('activo');
		dotprev.addClass('active');
	});
	$(".next").on('click', function(){
		var slideactual = $('.slider .caption .activo');
		var dotactual = $('.slider .frames .active');

		$('.slider .caption li').removeClass('activo');
		$('.slider .frames li').removeClass('active');

		var slidenext = slideactual.next('.lislidetexto');
		var dotnext = dotactual.next('.lidot');

		if(!slidenext.length){
			slidenext = $('.slider .caption li:first-child');
			dotnext = $('.slider .frames li:first-child');
		}

		slidenext.addClass('activo');
		dotnext.addClass('active');
	});
});